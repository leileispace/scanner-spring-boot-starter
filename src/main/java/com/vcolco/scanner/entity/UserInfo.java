package com.vcolco.scanner.entity;

import lombok.Builder;
import lombok.Data;

/**
 * @author lei
 * @create 2022-05-12 14:44
 * @desc
 **/
@Data
@Builder
public class UserInfo {
    private String userId;

    private String userIp;

    private Integer orgId;
}
