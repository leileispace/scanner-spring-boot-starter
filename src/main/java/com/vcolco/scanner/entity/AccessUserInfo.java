package com.vcolco.scanner.entity;

import lombok.Data;

/**
 * @author lei
 * @create 2022-05-12 14:44
 * @desc
 **/
@Data
public class AccessUserInfo {
    private String userId;

    private String userIp;

    private String extension;
}
