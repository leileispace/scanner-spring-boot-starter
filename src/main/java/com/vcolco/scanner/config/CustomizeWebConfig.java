package com.vcolco.scanner.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author lei
 * @create 2022-05-13 17:26
 * @desc
 **/
@Slf4j
@Configuration
@Order(1024)
public class CustomizeWebConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        log.info("加载用户请求拦截器!");
        registry.addInterceptor(new UserInterceptorConfig()).addPathPatterns("/**");
    }
}
